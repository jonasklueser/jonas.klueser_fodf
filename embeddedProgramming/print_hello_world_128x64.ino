#include "U8glib.h"
#include "String.h"

U8GLIB_ST7920_128X64 u8g( 13, 11, 10, U8G_PIN_NONE);

void draw(String s) {
  u8g.setFont(u8g_font_unifont);

  u8g.drawStr( 0, 22, s.c_str());
}

void setup(void) {
    u8g.setColorIndex(255);     // white
  
  pinMode(8, OUTPUT);
}

void loop(void) {
  // picture loop
  u8g.firstPage();  
  do {  
     draw("hello World!");
     
  } while( u8g.nextPage() );
}