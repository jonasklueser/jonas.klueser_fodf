#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include <string.h>

#define F_CPU 16000000

//LED Pin Output
//DDRB |= 0b00100000; is the same

void blink(int time)
{
    //LED blinks for one second
    PORTB |= (1 << DDB5);
    _delay_ms(time);
    PORTB &= ~(1 << DDB5);
    //delay after every char
    _delay_ms(500);
}

void showSign(int *word)
{
    for (int i = 0; i < 4; i++)
    {
        if (*word == 1)
        {
            //short blink
            blink(100);
        }
        else if (*word == 0)
        {
            //long blink
            blink(500);
        }
        word++;
    }
};
int morse[4];
//translates chars to morse code; 1 means short and 0 means
int *dictionary(char c)
{
    //make c to lower case
    if (c == 'a')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'b')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'c')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'd')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'e')
    {
        morse[0] = 1;
        morse[1] = 9;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'f')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'g')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'h')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'i')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'j')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'k')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'l')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'm')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'n')
    {

        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'o')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'p')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'q')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'r')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 's')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 't')
    {
        morse[0] = 0;
        morse[1] = 9;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'u')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'v')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'w')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'x')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'y')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'z')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else
    {
        morse[0] = 9;
        morse[1] = 9;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
}

int main()
{
    //wait at the start to get ready for watching
    _delay_ms(2000);
    DDRB |= (1 << DDB5);

    char *test = "huseyner";
    int len = strlen(test);
    for (int i = 0; i < len; i++)
    {
        int *code = dictionary(test[i]);
        showSign(code);
        _delay_ms(2000);
    }
}