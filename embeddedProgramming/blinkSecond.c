#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>

#define F_CPU 16000000

//LED Pin Output
//DDRB |= 0b00100000; is the same

int main()
{
    DDRB |= (1 << DDB5);
    while (1)
    {
        //digital bin to high
        PORTB |= (1 << DDB5);
        _delay_ms(1000);

        //setting deigital pin to low ; flipping the bits with and sign and tilde 
        PORTB &= ~(1 << DDB5);
        _delay_ms(1000);
    }
}
