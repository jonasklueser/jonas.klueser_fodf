#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>

#define F_CPU 16000000
#define ARDUINO_AND_LED_PORT PORTB //register for arduino and led
#define ARDUINO_PIN PIND            //register for arduino input
#define ARDUINO_BIT PB5             //bit for arduino input
#define LED_BIT PD1                 //bit for button input/output
#define LED_DDR DDRB                //LED Data direction register

int led_on;

int main()
{
    led_on = 0;
    LED_DDR = _BV(LED_BIT);
    ARDUINO_AND_LED_PORT |= _BV(ARDUINO_BIT);

    while (1)
        if (signal_recieved())
        {
            switch_led();
            _delay_ms(1000);
        }
}

int signal_recieved()
{
    if (bit_is_clear(ARDUINO_PIN, ARDUINO_BIT))
    {
        return 1;
    }
    return 0;
}

void switch_led()
{
    if (led_on)
    {
        ARDUINO_AND_LED_PORT = 0b00000001;
        led_on = 0;
    }
    else
    {
        ARDUINO_AND_LED_PORT = 0b1000000;
        led_on = 1;
    }
}