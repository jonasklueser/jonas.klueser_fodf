#include <stdio.h>
#include <inttypes.h>
#include <string.h>

int morse[4];
/*
void showSign(int *word, int length)
{
    for (int i = 0; i < length; i++)
    {
        if (*word == 1)
        {
            //blink
        }
        else if (*word == 0)
        {
            //wait
        }
        word++;
    }
};*/
int *dictionary(char c)
{
    //make c to lower case
    if (c == 'a')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'b')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'c')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'd')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'e')
    {
        morse[0] = 1;
        morse[1] = 9;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'f')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'g')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'h')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'i')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'j')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'k')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'l')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'm')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'n')
    {

        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'o')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'p')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 1;
        return morse;
    }
    else if (c == 'q')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'r')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 's')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 9;
        return morse;
    }
    else if (c == 't')
    {
        morse[0] = 0;
        morse[1] = 9;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'u')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'v')
    {
        morse[0] = 1;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'w')
    {
        morse[0] = 1;
        morse[1] = 0;
        morse[2] = 0;
        morse[3] = 9;
        return morse;
    }
    else if (c == 'x')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 1;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'y')
    {
        morse[0] = 0;
        morse[1] = 1;
        morse[2] = 0;
        morse[3] = 0;
        return morse;
    }
    else if (c == 'z')
    {
        morse[0] = 0;
        morse[1] = 0;
        morse[2] = 1;
        morse[3] = 1;
        return morse;
    }
    else
    {
        morse[0] = 9;
        morse[1] = 9;
        morse[2] = 9;
        morse[3] = 9;
        return morse;
    }
}

int main()
{
    //wait at the start to get ready for watching

    char *test = "abc";
    int len = strlen(test);
    for (int i = 0; i < len; i++)
    {
        int *code = dictionary(test[i]);
        
        printf("%i\n", code[0]);
    }
}