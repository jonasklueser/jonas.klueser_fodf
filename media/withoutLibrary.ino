// example sketch MPU-6050
#include<Wire.h>
#include <SoftwareSerial.h>

#define PIN_WIRE_SDA         (26u)
#define PIN_WIRE_SCL         (25u)

SoftwareSerial mySerial(PD1, PD0); //RX, TX

const int MPU= 0x68;
int16_t AcX, AcY, AcZ, GyX, GyY, GyZ;

void setup() {
    Wire.begin();
    Wire.beginTransmission(MPU);
    Wire.write(0x6B);
    Wire.write(0);
    Wire.endTransmission(true);
    mySerial.begin(9600);
}

void loop() {
  Wire.beginTransmission(MPU);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU, 14, true);

  AcX = Wire.read()<<8|Wire.read();
  AcY = Wire.read()<<8|Wire.read();
  AcZ = Wire.read()<<8|Wire.read();

  GyX = Wire.read()<<8|Wire.read();
  GyY = Wire.read()<<8|Wire.read();
  GyZ = Wire.read()<<8|Wire.read();

  mySerial.print("Accelerometer: ");
  mySerial.print("X = "); mySerial.print(AcX);
  mySerial.print(" | Y = "); mySerial.print(AcY);
  mySerial.print(" | Z = "); mySerial.print(AcZ);
  
  mySerial.print("Gyroscope: ");
  mySerial.print("X = "); mySerial.print(GyX);
  mySerial.print(" | Y = "); mySerial.print(GyY);
  mySerial.print(" | Z = "); mySerial.print(GyZ);
  mySerial.println(" ");
  delay(5000);
}
